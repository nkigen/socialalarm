-module(myhandler).
-include("/home/nkigen/development/yaws/yaws/lib/yaws/include/yaws_api.hrl").
-export([out/1]).

out(Args) ->
	Uri = yaws_api:request_url(Args),
	Path = string:tokens(Uri#url.path, "/"),
	out(Args,Path).

out(Args,Path) ->
       Version = lists:nth(3, Path),
       Method = (Args#arg.req)#http_request.method,
	if
		Version == "v1" ->
			v1:serve_request(Method,Path);
		true -> 
			{html, Version}

	end.

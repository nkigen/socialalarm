-module(v1).
-include("/home/nkigen/development/yaws/yaws/lib/yaws/include/yaws_api.hrl").
-export([serve_request/2]).

serve_request(Method, Path) ->
	try
		Request = lists:nth(4,Path),
		case Request of
			"users" ->
				if
					Method == 'GET' ->
						{html, "Users using GET"};
					true ->
						{html, "Users uknown method"}
				end;
			_ ->
				{html, "unknow option used"}
		end
	catch
		_:_ -> false,
		{html, "ERROR Unknown  url!!!"}
	end.
